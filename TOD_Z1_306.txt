1. Ferdinand Porsche se měl stát klempířem.  Po základní škole se Ferdinand Porsche stál učněm v otcově dílně.  Ferdinantův bratr Anton měl původně firmu převzít, ale při prace zemřel.
 
2. Ferdinand zařídil elektrickou laboratoř, v níž experimentoval s elektřinou.  Když otec byl pracovně pryč, Ferdinant instaloval v rodném domě elektřinu.  Díky tomu, otec vyjádřil svému synovyi respekt a dovolil odejít do učení do firmy Bély Eggera ve Vídni.
 
3. V osmdesátých letech 19. století se elektřině dělala zatím spíše jen reklama a předváděla se, protože s ní lidé ještě ne uměli bezpečne zachazet.  Dalším problémem bylo financování.
 
4. Ferdinandova sestra Anna byla rovněž zaměstnána ve Vídni, a tak mohla svému bratrovi ,,vést domácnost“.  Porsche měl proto stále víc času.
 
5. Ferdinand Porsche se stal vedoucím zkušebního provozu a prvním asistentem v oddělení výpočtů.
