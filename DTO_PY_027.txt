Můj víkend.
 
O víkendu spím dlouho, často vstavám až v dvanáct hodin.  Po snídani jdu na prochazku do parku.  Někdy píju kávu v kavárně.  Ráda čtu, a proto když se vratím domu čtu zajimavou knihu.  Taky ráda vařím a někdy připravuju nějaké chutné jídlo.  Večer se dívám na televizi a pak jdu spát.
