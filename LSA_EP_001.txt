Nedele
 
Vlak zpoždoval se, nieni dost‘ čas na jet‘ do koleji.  Jedu na Univerzitu.  Na Albertove u brany nejaky muž řika, že dnes všechno zavřene.  Je to moje chyba?  Ne, je ješte brzo, nakonec příchazí jiní studenty.  Všechno v pořadke.
 
Jedu do na koleji přes cele mněsto, tram jede půl hodiny.  Blizko koleji roste hezkeí strom: sosna nebo limba.  U stromu leži šiški.  Běru si jednou, na ščastěi.
 
Jede tam čislo jedna, to dobrí spůsob poznat mněsto.  V kavarne davam si neco, a piju bíle vino.  Ne chutna mi, přilis suche.  {V metru vidim narkomany.}<in>  Zrana pršilo, večer zase prší, rychle vratim se do kolejí.
 
Pondelí
 
Z rana četla jsem knihu Terezy Boučkove, pak jela jsem tramvají a hledala, kde by možna se nasnidat‘.  Zajmava cesta přes Andil, ale snidani našla jsem teprve kole školy.
 
Prve vyučovani, není tak těžko, jak jsem se bala.  Po vyučovaní jedu negkde, kde jsem ješte nebyla.  Stacje Metra Křižikova na Karline.
 
Obedvam v hospode, hovezi pečení a knedliki.  Rezane pivo mí chutna.  Pak přechazim se.  Krasny Kostel Svatych Cirila a Metodeho vystaven v roce 1863 v tysiclete krztu Ceska.  Kostel je neoromansky, prekrasny oltař nakyty papieźskou Korunou.  V roce 2002 byla tam povod‘, taky v Kostele.
 
Odpoledne - setkani z Terezou Boučkovou.  Prezentovali jen její nove knihy.  Večer na {Karluv}<in> Mostu.  {Nejaki hrajec hral}<in> klassickou hud’b‘u na harmonii.  Marne bile vino v hostinci.
 
Utery
 
Počasi se (popsuĺo) poškozavalo.  Je zima, občas prši.  Rano pojele jsem na Anděl.  Našla jsem tam směšnou {plenerovou}<in> vystavu vynálezí ktere odměnili żivot žen: odkunecz, binstonosz itp vysavač, automaticka pračka, žehlička, {vysoušeč vlasů}<in>, a taky rtenka, rašenka, {kalhotovy kostim}<in>, podprsenka, spivala tusz stylonové půnčochy.  Velike makety leži na (driedzinku) nádvoří velke budovy kina Anjel<.>
 
Po vyučovani objedvala jsem v hospode: gulaš a knedliki.  Samožemě taky pivo.  V nakupne galerie podivala jsem se na boty znački Bata.  Navště{vil}<in>a {jsem}<in> antikvariat nic {zajmavého}<in>.  Odpoledne setkani z ližovačkou Kateřinou Neumanovou.  Večer v kavarne, ktere vypada na antikvariat, {galerie}<in> nebo muzeum.  Ve vitrině (vypchane) zviřata, na polici knihy, hry, stare globusy a pisaci stroje.  Školne instrunenty pro pokusy na vyučovani fizikí a chemi.  Dala jsem jablečne pivo - cider.  Chutnalo mi.
 
Středa
 
Zrana poznavala jsem okol{i|í} naše koleji.  Pošla jsem z kopce Vetrnik dolí přes lesni uvoz.
 
Po vyučovani byli jsme v kině na filme a setkani s Jiřim Mentzlem.
 
Film Postřižiny jsem už dřive videla, ale jsem ho {přiliš}<in> ne pamatovale<.>
 
Objedvala jsem v hospode, poprve bramborovy knedlik.  Konců navštevovala jsem Umelecko průmyslove muzeum.  Rada bych udelala snimky, ale se ne smí.  Jsou tam vyborne kniźní vazby, stare a nove, dobře popsané, a Není k tomu otdeleni z‘adny katalog.
 
Večer byla jsem s Adamem na konzerte Marty Kubišove a Lucie Bily.  Adamovi se libilo, {pro}<in> mě nic moc, ale těšim se, źe jsem zname zpievački uvidela a uslyšela.
 
Čtvrtek
 
Rano brzo přišla jsem do školy a začala jsem se připraviat k prezentacjí - hledala jsem materialy v internetu.
 
Po vyučovani byli jsme spolu v Muzeu Alfonsa Muchy.  Bylo to zajmave, proto że jeho díla jsem vżdy měla rada.  Teď jsem se moc dozvedela o jeho životě a tvůrcí dr{o|a}ze.  Potom byli jsme spolu v restauracjí, kde je maly pivovar.  Pivo bylo skvěle.
 
Dostala jsem napad, że ženy na jeho plakatach jsou stejne jak hrdinka filmu, ktery jsme videlí včera, secesní dama, ktera nakonec zaněchava stary styl a modernizuje se.  Mucha zustal navżdy stejny.
 
Večer - prohazka.  Videla jsem pasaž Lucerna a umelecke dílo Davida Černiho - krale na obracenym kůní.
 
Patek
 
Zrana hledam hrb Karla Kryla na Břevnovskem hrbětove.
 
Po vyučovaní mame volne odpoledne.  V hospode v okoli školi davam se smaženy syr a pak navštevuju Botanickou zahradu a vystavu historickyich hraček v Emauzsym Klašteře.
 
Potom nakupuju neco k jidlu, a jdu dal‘ seznamovat se z městem.  Jsem na plaží u Vltaví, a potom piju {nejlepši ve svete}<in> pivo v Novometskem pivovaru.  Pak chodim po měste, jsem u Muzeu Kafkí kde vidim druhoue dilo Davida Černego - čurajíci muže.
 
S Karlův mostu divam se na představeni, ktere se odehrave na scéne na řece.
 
Súboto
 
Vylél do českeho raje.
 
Nedele
 
Zrana byla jsem v kostele v Břevnovskem kl{o|a}steře.  Pak navštevovala jsem ořechovsky židovsky hřbitov.  Je tam hrób Franze Kafki.  Hřbitov je hezky, co bylo pro mě překvapenim - často na hrobach neni žadne naboženske emblemy.  Potom byla jsem na Vinohradach.  Přijela jsem na stanici Jiřego z Podebrad, o ktere szpival Jarek Nohavica, videla jsem Kostel Srdce Pana Jezuse, neobvykla konstruktivisticka architektura.  Podivala jsem se na televizni vež, na kterou lezí miminka.  Pak šla jsem na Sivecovou ulicí, podivat se na pomník Ryszarda Sivca.  Odpoledne bylo vedro, ukryla jsem se v internetove kavarne, a potom snila jsem zmrzlinu a sidela u fontany.  Večer byla jsem na Vyšehrade.  Chram byl uź bohužel zavřen, podivala jsem se na hřbitov a na hezky pohled na město.
 
Pondelí
Po vyučovaní jela jsem do lokti na vystavu {v XXX}<in> dil rodu Sobotovych.  Cesta ne byla mila.  Nejdřiv na nadraži se pokazalo, že neni jizdenki, a musim čekat na druhy autobus.
 
Behem cesty kazjilo se počasi, pršilo, a ja jsem ne měla deštnik aní bundu.  V Karlovych Varech ne mohla jsem dozvedet, kdy budu mit spojení, musela jsem čekat así hodinu, a jsem se dozvedela, że není žadne jizdenki na zpetnou drohu do Prahy.
 
V Museum v Loktiu setkala jsem se ze zmamymí, a to bylo už moc mile.  Podivali jsme umelecke knižni vazby, {bavili se}<in>, hrala kapela.  Pak šli jsme do hospody.  Večer Eva pozvala nas do sve zahrady, udelala ohen v kominku.  Bavili jsme se a pili různe alkohole asi do půlnocy.
 
Utery
 
Ubytovala jsem se u přateli v Lokti.  {Rano se jsem mohla podivat na dilnu a nadhernou vybavu.}<in>  Po snidani, přijela jsem do Prahy.  Jela jsem s jejích znamou, ktera přijela z Prahy autem.  Behem cesty, asi dve hodiny jsme bavílí, proto że mamy něco společneho - a to zajem o knižni vazbu.  Mluvili jsme takí o vychove déti, proto źe ona ma až čtiří déti.  Nestihnoula jsem vyuku, ale odpoledne pošla jsem na konzert Pavla Dobeše. do klubu Reduta.  To mi se libilo, spiva podobně k Nohavicy.  Bylo mi jen lito, že, jak to vyplynoulo z jeho odpověde, ne mají se radi s tim mojim oblibenym pisničkařem.
 
Středa
 
Rano {byli jsme -> jsme byli} v kine a videli jsme film 3 sezony v pekle, setkali jsme se z hercem Kryštofem Hadkem.  Film odehraval se o doby totality a moc me dotknoul.
 
Po vyuce {byli jsme -> jsme byli} na prochazce v {pražskem}<in> židovskem měste, prohliželi jsme synagogi a {na}<in> židovsky hřbitov.  {Moc}<in> Zájmave bylo to źe synagogi byliy různy, ty stare, ortodoksní byly skromný a posled‘ia spanelska moc dekoratívni.
