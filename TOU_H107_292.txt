Čeština a já
 
Poprvé jsem se seznámila s češtinou v létě 2008.  Tehdy {byla jsem -> jsem byla} v Praze na letním kurzu češtiny pro cizince.  Musím říct, že bylo to zajímavé.
 
Byl to nový jazyk pro mě, ale mohla jsem něčemu rozumět, protože některá slova byla stejná jako v ruštině.  Nejenom jsem se seznámila s češtinou, ale i s celou Českou republikou.  Protože jsme hodně cestovali, poznávali českou kulturu a historii.
 
Myslím, že čeština mě zapojila do sebe svým rozdílem s ruštinou, nezvyklou výslovností, některými hezkými slovy a svou melodií.
 
Proto po návratu domů jsem ji trochu začala studovat.
 
Nejdříve jsem četla české pohádky.  Pak jsem zkusila číst učebnice, ale to bylo trochu těžké a neměla jsem na to hodně času.
 
Proto když jsem přijela do Prahy, tak už jsem něco mohla říct.  Pamatuju, že v první den jsem šla s maminkou někde v centru, a nemohly jsme najít jednou ulici.  Zeptala jsem se jednoho pána.  A on byl tak hodný, že to nám ukázal a pak se mě zeptal kolik už bydlím v Praze.  Řekla jsem, že je to můj první den.  Ale ten pán řekl, že nevěří, protože už mám dobrou češtinu.
 
Tak myslím, že ten případ byl jako znamení pro mě, abych hodně studovala a mohla mluvit jako češi. 
