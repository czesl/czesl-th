Prostě skvělé prázdniny.
 
Moji nejlepší prázdniny byly předloni.  Začátkem skončila jsem školu a měla jsem maturitní večírek.  Je to bylo krasně!  Všechny holky byly v šatech a chlapci v kostýmech.  Měla {jsem}<in> na sobě růžové a béžové šaty.
 
Moje první cíle byla dostat vizu a v červenu navštivila jsem český konzulat v Astaně.  Pak odjela jsem se svou rodinou na dovolenou na jezro Issyk-Kul.  Byly jsme tam {až}<in> měsíc.  Po odjezdu mých rodiče, moji kamaradové navštivily mně.  Chodily jsme na procházky, plavaly jsme v jezeře, opalovaly jsme se na pležu, chodily jsme do pležových party.
 
Celkem, všechno bylo v pořádku.  Potom odjela jsem domů a začála připravovat se k odjezdu do Prahy.  Koncem srpna dostala jsem vizu a odjela do Prahy.  Měla jsem prostě skvělé prázdniny a letos těším se na nich!
