Ahoj Stéphane!
 
To je fakt super, že konečně mužeš přiletět do Prahy.  Já jsem si povidal na internetu až bude tvuj letadlo v Ruzyněi.  Když přiletí včas, v Pátek v 16 h 35, asi mužes jet autobusem do stanice metra Dejviscká.  Od stanice Dejvická, pojedeš metrem do stanice Malostranská.  Tam blízko, v kavarné ”U Dobré Nalady“ budu čekat na tebe.  Asi se sejdeme tam kolem 17 h 30.  Budeme pít dobré české pivo a jíst domaci gulaš.  Až jsme snedli gulaš a vypili pivo, půjdeme na prochazku v historickém centru.  Samozrejme budeme se dívat na praský hrad z karluvého mostu.  Je to moc hezké v tmě.  V sobotu budeme navstěvit praský hrad a večer půjdeme do narodního divadla.  Domluvíme ještě spolu, co uděláme v Neděli a další dni.
 
Těším se moc!
 
Adam.
