Souhlasím s tím, že České silnice jsou nebezpečné.  To je způsoben nejen z toho, že češi řídí nebezpečné, ale i z toho, že tady nemáte dost široké ulice a chodníky.  Myslím si, že nějaký telivizní kampaň, který učí lidem jak neriskovat bude trochu pomoct, ale hlavní problem tam pořád je.  I když řízení je dost {velký}<in> problem v České republice věřím tomu, že tady máte větší {problém}<in>, který je důležitější vyřešit.
 
Se svou skušeností vím, že hodně ulic v České republice jsou nebezpečné.  Zdá se mi, že ulice tady nemusí mít specifickou minimálnou širokost.  Taky, nějaké chodniky ve vesnicích i v Praze často nejsou dost široké, aby někdo mohl projít bez problémů s kočárkem.  Nejhorší je v zimě, když padá hodně sněhu a nikdo to nemá kam dát, tak to prostě hazí na chodniky a pak lidé nemájí kde chodit.  Nemůžou chodit na ulici, hlavně když májí kočárky, protože všichni ví, že se tady v České republice hrozně rychle řídí.
 
(text je pokračován na druhém papíru)
 
Tak můj názor je prostý.  Ten telivizní kampaň je dobrý nápad, a možná bude mít nějaký vliv na počet lidí, kteří umírají na ulici v České republice.  Avšak nejlepší by bylo, jestli někdo vymýšlí nějaký způsob jak udělat, aby ulice a chodniky byly širšší.  To by mělo mnohem větší vliv na počet mrtvých lidí, a taky by udělalo, aby bylo přijemnější chodit pěšky po Praze.
