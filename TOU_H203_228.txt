Češi a Rusové
 
Jaký je rozdíl mezi těmi národy?  
 
První věc je jazyk, ale to je jasně.  Naši jazyky mají stejné kořeny.
 
Druhá věc je kultura a mentalita.  To záleží na naši historii, na prezidentech a králech a na podnebí, myslím.
 
V té kultuře můžeme najít hodně různých větví.  Například, jídlo (nebo potraviny).  Mnohým rusům se nelibí nechutná české tradiční jídlo, a taky mnohým čechům ruské jídlo vypadá divně.
 
Rozdíl je v poměrech k světovým problémům (ČR je v Eu), v svátkách svátcech; ve vzhledu, v myšlenkách, v cenech, v kvalitě, v počtu: ve všem, už nevím, co ještě tady připojít.  Prostě jsme už různé, svět se změnil dávno.
