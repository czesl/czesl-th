Milá kamarádka                                                                            
 
18.11. 2009
 
 Chtěla bych něco říct o Rostově.  Je to mé rodné město.  Tam teče řeka, která se jmenuje Don.  Žije tam hodně lidé.  Myslím, že Rostov je dost velké město.  Líbí se mi mé město, protože je hezké.  Tam je všechny důležité obchody: knihkupectví, samoobsluha a td.  Také tam jsou krasné parky, moderní a historické budovy, kina, divadla a hodně obchodních
domů.  V centru je velké náměstí.
 
Doufám, že budeš mít čas a přijedeš ke mně.  Když přijedeš, půjdeme na procházku a prohlédneme si všechna zajímavá místa.
 
Těším se na tvůj dopis
                                                       
Tvá Ksenia
