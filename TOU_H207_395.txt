„To se stalo kdy vládl hrachový císař“
 
V Ruském jazyce je hodně ustálených výrazů.  Cizinec samozřejmě stěží porozumit timto idiomatickým projevům, protože je nutné znát jejich historie a vývoj.  Kromě toho každý ustálený výraz úzce souvisí s kulturou státu a jeho národem.  Výrazy se zakořeňují v jazyce a nikdo obvykle přesně nemůže {popsat}<in> historie (jeho vzniku. 
 
„To se stalo kdy vládl hrachový císař“ znamená, že to bylo velmi dávno.  Existuje několik verzí vzniku tohoto výrazu.  První připomínka hrachového císaři byla v 19<.> století.  To byl článek „O hrachovém císaři“, který byl napsán absolventem Moskovské uUniverzity.  V tomto článku autor se zabývá historií a se snáži porozumět skutečnému významu.
 
Hrachový císař vznikl z národních tradičních pohádek.  Jedna z nich začíná: „… Pradávno na světě žili rusalky, lesní muži, čarodeji, nad nimi vladl hrachový císař…“.  To znamená, že existuje určité spojení s pohadkami a hrachový císař je zároveň nereálný jako ostatní báječné bytosti.  Z toho vyplývá, že když použiváme tento výraz, zdůrazňujeme, že to nevypádá uvěřitelne.  Ale lze projednat ještě jeden názor na vznik idiomy.  Někteři filologové řikají, že ten výraz je prostě předěláním Řecké přísloví – Presbytenos  To znamená „je starší než Kodr“.  Kodr – to je mytický císař Attiky Řecko).  Je možné, že někdo vyměnil jmeno Kodros.  Výslovnost Řeckého jmena „Kodros“ je podobná ruskému jmenu „Horoch“,  a proto ta změna mohla by být.
 
Nicméně nekteré kombinace slov také stojí za to, aby {jim}<in> dávali ozor.  Napřiklad 
<li> hrachová paměť – špatná paměť 
<li> hrachový strašák – hloupý člověk (urážka) 
<li> hrachová slova – nesmysná slova
 
To může pomoct v porozumění skutečného významu přísloví  V ostatních jazycích také existuje hodně výrazů o zapradávné době.  V slovanských jazycích např. Poláci mají „Za Krófa Cwiezka“ – to znamená dobu, kdy vládl král Cwiezek.  Češí mají výraz Za Marie Teremtete“, ale moc nepouživají to.  Když podíváme do slovniku – „teremtés“ označuje „výtvor, bytost“.  Nemci mají výraz „Anno Tobak“, který je předěláním latinské přísloví – Anno Domini.
 
V Anglii pouzivají „in days of yere“ nebo „in days of dot“.  Někdy můžeme spojít tento výrazy s realními fakty.  Takovým je francouzský výraz „au temps du roi Dagobert“.  Dagobert je / byl realním, skutečným císařem v 17 století.  Angličane řikají „when Queen Anne was alive“, a to je spojené s historií.      
 
 
