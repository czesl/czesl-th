Dobrý den!
 
Jmenuji se Eva.  Jsem jedenadvadcet let.  Narodila jsem se v Moskvě a teď bydlím v Praze.  Mám ráda svetovou literaturu, psy a prochazky.  Ráda piju kavu a mám chuť na krokety.  Doufam že po skončení tohoto kurza se zapíšu na Karlovu Univerzitu a budou mít hodně dobré kamarady.
