Nižegorodský hrad.
 
V historickém a administrativním centru města Nižnij Novgorod je starodávná pevnost.  Ta pevnost - Nižegorodský hrad.  Už pět set let stojí a ani jednou ho nedobyli nepřátelé.  Teď, samozřejmě, ten hrad už nemá válečný význam.  V současné debě tudy je administrativní budovy a muzeumní objekty.
 
Historie Nižegorodského hradu nerozborně spojována s historií města Nižnij Nogorod.  To město bylo založeno v roce 1221.  Tehdy v něm byla postavena dřevěná pevnost.  Brzy nastal těžký čas - tataři napadli Rus.  Dřevěnou pevnost ne jednou dobyli nepřátelé.  Proto v roce 1508 byla začíněna stavba kamenného hradu.
 
Co je Nižegorodský hrad? To je krásná a starodávná pevnost z červeného cihlu.  Ten hrad leží na kopcích.  Svého času to má důležitý válečný význam.  Z těch kopců otevírají překrásné pohledy na druhý břeh Volgy.
