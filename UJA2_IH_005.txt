Ahoj, Adame!
 
Mám skvělí nápad!  Já chcí tě pozvat na nejlepší muzikál „Tajemství“, který uvádí divadlo „Kalich“.  Toto divadlo se nachází v Praze 1, v Jungmannove ulici.  Mohl bys ses tam dostat tramvají číslo 9 do stanice „Dívčí škola“.  Já už mám vstupenky, koupila jsem je včera v pokladně divadla.  Jedna stála 495,− Kč, ale dostaneš ji jako dárek.  Když mě podporuješ, tak sejdeme se u divadla v pátek v 19 45, ale já ti zítra určitě zavolám.
 
P. S. Už se těším.  Měj se hezky.
 
Čau.  Eva.
 
