Bít „baklušy“ nebo chytat lelky?!!
 
Když někdo se fláká, jemu občas říkají: „Přestaň bít baklušy!“  Co je to za podivné obvinění?  Co jsou ty „baklušy“ a kdo i kdy je bíje?
 
Od dávných dob drobní výrobci produkovali ze dřeva lžíce, hrnky a další nádobí.  Aby vyřezali lžíci z kousku dřeva, pro začátek museli odseknout třísku z polena.  Tá tříska se přesně tehdy jmenovala „bakluša“.  Baklušy museli připravovat tovaryšé.  To byla snadná a bezvýznamna činnost, která nepožaduje spoustu znalosti.  Té přípravě třísek se říkalo „bít baklušy“.
 
Z toho, že mistři říkali svým žákům „baklušečnici“, vzniklo přísloví „bít baklušy“.
 
Česky se to většinou řekne „chytat lelky“.  Lelek je pták.  V Čechách žije lelek lesní.  Je velký jako kos, hnědý a poměrně nenápadného vzhledu.  Má velké oči a hubené nohy.  Loví drobný hmyz, ale až se začne stmívat.  Jinak sedí jak přikovaný celý den na větvi stromu a ani se nehne.  To dobře věděli ptáčnicí, kteří v dřivějších dobách chytali ptáky na lep.  Na tohle chytání bylo nezbytné vodorovné prkénko na nožičce, které se natíralo lepem a davali tam návnadu.  Lep nedal ptáku odlétnout.  Ptáčnici věděli, že se nikdy nedá chytit lesního lelka, protože přes den seděl na větvi, v noci chytal hmyz.  A to známena, že se lepu vždycky vyhýbal.  A proto lelka lesního nelze chytit.  Ten co „chytá lelky“ neděla prakticky nic.  Ale jinak ještě v ruštině máme přísloví „chytá vrány“.  <img =)>
