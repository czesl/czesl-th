Milý Petře!
 
Máš nějaké plany o víkendu?  Chtěla bych tě pozvat na výlet do Domažlic na „Chodské slavnosti“.  Ty slavnosti se budou konat 13. - 15.8 2010 a proto můžeme návštivit spousta zajímavých památek a prohlednout krasné místa v Chodsku.  Slyšel jsi něco o folkloristických festivalech v ČR?  Myslím, že to vypadá velmi zajímavě.  Mají bohatý program, který se {nám}<in> pomůže seznámit s chodskou kulturou, budeme poslouchat tradiční hudbu a {se dívat -> dívat se} na taneční soubory.  Kromě toho půjdeme nakupovat do staročeského jarmarku v centru a ochutnáme slavné chodské jídlo.  Stojí za návštěvu Chodský hrad a Kozinův statek na Ujezděu.  Podle mého názoru, Chodové jsou velmi dobré a lhosté lidé, kteří budou rádi mluvit o své kultuře.  Ty lidé mají bohaté dějiny a zajímavé tradice.
 
Čekám na tvou odpověď.  Jakmile se rozhoduješ, zavolej mi.
 
Tvá Eva
