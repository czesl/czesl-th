Výstavy a veletrhy ukazují uspěchy ekonomiky celé země<.>  Jsou formou propagace vyrobků z různých oblastí hospodářsk. a zemědělství (strojírenské vyrobky, živočišná výroba)<.>  Jubilejní všeobecná zemská výstava v Praze roku 1891 je první průmyslovou výstavou.  Byla oslavou české práce a ekonomiky celé země a měla obrovský úspěch.  Na všeobecné výstavě v Praze obdivovali návštěvnici obrovský výbor zboží z různých oblastí.
 
Měli možnost zakoupit vyhlídkový let balonem a obdivovat všechno z ptačí perspektivy.  Také vystupovali na Petřínskou rozhlednu.
 
František Křižík řídel výstavu a byl autorem znamé světelné fontany.
 
Jubilejní všeobecná zemská výstava byla manifestaci vyspělosti českého průmyslu.
