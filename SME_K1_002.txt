Moje nejoblíbenější kniha
 
Ráda čtu ruskou a zahraniční literaturu.  Jedna s mých nejoblíbenějších knih je „Zločin a trest“ Dostojevského.  Fedor Michajlovič Dostojevskijí je ruský spisovatel 19 století, který psal v duche realismu.  „Zločin a trest“ je román, který povídá o mladém studentu Rodionu Raskoľnikovoví, který potřeboval peníze.  V jeden den on zabíl sekerou stařenou, která půjčovala peníze, a její sestrou, protože nemohl vrátit peníze ve lhůtě.  A pak Raskoľnikov měl hrozné duševní muky.  Vyšetřovatel myslel, že Rodion vykonal {vraždu|vzaždu} a flačil na neho psychologický.  Raskoľnikov nacházé záchranu v Soněčke.  Je to mladá dívka, s těžkým osudem, která prodává své tělo.  Ona věřila v Boha a zachránila Raskolnikova.
 
Raskolnikov se přiznává v zločine a je odeslál na {nucené}<ni> práce, a Soněčka jela za ním.
 
To je velký a krasný psychologický román, který byl napsán velkým spisovatelem.
