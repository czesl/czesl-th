Prostě skvěla prazdniny
 
Jak jsem stravila prazdniny?
 
Každé léto jezdím do Řecka.  Můj strýc bydlí v Řecku už patnáct roků.  Taky v gimnazium jsem učíla řečtinu.  a to je dobrá možnost pro mně, protože můžu poznat nové kultury i zdokonalovat řečtinu.
 
Nejzapomenutelnější léto bylo když můj bratranec se rodí.  To léto bylo plno se zabavnými přihodámi.
 
Mám mnoho přátel v Řecku.  Když jsem tam děláme program na celého týden.  Každé ráno jdeme k moři, potom se prochazíme po paru.  V sobotu a v neděli jezjdéme k jnějakým ostrovům a spíme v stanech.  Neuvěřítelné prožívání.
 
Mám ráda moře a pro mně je nejlepší prazdniny
