Tělesný vzhled a povaha.
 
Jedno české přisloví řiká: „Kolík hlav, tolík smyslů“.  To ovšem platí i pro tělesný vzhled a povahu člověka.  Mám tři dobří přátelé, ale každý se od sebe navzájem liší ve vzhledu a charakteru.
 
Adam je hubený, má lehkou a energickou chůzi.  Adam hodně je tlustý a nemotorný.  Moc hezká je Eva.
 
Má vysoké čelo, modré oči, dlouhé řasy, rovný nos, velký ústa a půvabný úsměv<.>
 
Vlasy má světlé a vždy dobře učesané<.>  Každý den dělá různý učesy.
 
Eva má příjemnou povahu.  Je to hodná holka, je upřímná, veselá a štědrá.
 
Líbí se mí rozumné, čestné a dobrosrdeční lidé.  Myslím si, že lidé rozdílnych povah nemohou být dobrými přáteli.
