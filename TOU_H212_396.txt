Jablko svaru.
 
Frazeologismus – je ústalá fraze, ktera je použivánáa v různych situacích různými lidmi, ale má jedné tema.
 
Jablko svaru – je frazeologismus, který známe ze řecké mifologie.  Znamená něco, co je důvodem sporu nebo nepřátelství mezi lidmi.
 
Původ rčení:
 
Bohyně svaru Eris nebyla pozvána na svatbu bohyně Theris a krale Paleq.  Za to se pomstila tím, že do salu vhodila jablko s nádpisem „Te nejkrasnější.“  Chtěly ho dostat tři bohyně: Athena, Hera a Afrodita, a proto požádaly trojského prince Parida, aby vyřešil, komu jablko dát.  Každá bohýně začila slibit princovi něco a svými sliby ovlivňovat.  Hera, manželka boha Dia, nabízela bohátství, bohyně moudrosti a války Athena slibovala chytrost a slavu.  Třeti – bohyně lásky Afrodite slibila soudci nejkrasnější ženu světa, jestli ji zvolí.
 
Jablko dostala Afrodita.  Ta slib svůj splnila a pomohla Papidovi únest ze Sparty slíbenou krasnou Helenu.  Nasledkem toho potom došlo k Trojanské valce.
 
Teď „jablko svaru“ nazýváme věc, kvůli jejiž se hadáme a nemůžeme vyřešit, kdo má pravdu.
 
Napřiklad dřivé „jablkem svaru“ bylo náboženství.  Staty nemohly mezi sebou výřešit, v koho musi věřit lidé a jaké božské zákony je třeba přijat.  Kvůli různým názorům na náboženství se probíhalo hodně sporů a valek.  I teď tema náboženství může být důvodem napjatých poměrů.    
