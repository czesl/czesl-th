Domácí úkol
 
1) Vrať se v 10 hodin! – Nevrat se bez dobrých známek.
 
Chod na tenis! – Nechoď do klubu!
 
Uklaď se už v 9! – Neklaď se pozdě!
 
2) Napište úkol! – Nepište chyby!
 
Udělejte cvičení! – Nedělejte chyb!
 
Mluvte česky! – Nemluvte rusky!
 
3) Zaplaťte u pokladny! – Neplaťte kartou!
 
Zkušte tu bundu! – Nezkušte ten svetr!
 
Neberte polevku! – Vezmete  dort!
 
4) Počkejte na mně! – Nečekej na mně!
 
Poďte sem! – Nejdětě sem!
 
Zavolejte mi, když půjdete na procházku!
 
Nevolejte mi pozdě!
 
5) Řekněte Pavlovi ať udělá domácí úkol<.>
 
-Nevolejte mi pozdě!  Neřekejte Pavlovi ať udělá …
 
Předejte Martině ať určitě napíše omlouvenku!
 
Nepředejte Martině at určitě….
 
Oznámte Petře ať přijde zítra<.>
 
Neoznamujte Petře at přijde zítra<.>
